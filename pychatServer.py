#curses setup
import curses
import math
def rawInput(stdscr, r, c, prompt_string):
    curses.echo()
    stdscr.addstr(r, c, prompt_string)
    stdscr.refresh()
    input = stdscr.getstr(r + 1, c)
    input = str(input)[2:-1]
    return input
def winMsg(stdscr,string,scrollAmount,row,col):
    stdscr.scroll(scrollAmount)
    stdscr.addstr(row,col,string)
    stdscr.refresh()

stdscr=curses.initscr()
rows, cols = stdscr.getmaxyx()
curses.start_color()
curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_GREEN)
curses.init_pair(2, 17, 8)
curses.init_pair(3, 1, 180)
curses.init_pair(4, 3, 4)
curses.init_pair(5, 1, 2)
inputWin=curses.newwin(2,0,curses.LINES-2,0)
infoWin=curses.newwin(1,0,0,0)
mainWin=curses.newwin(int(math.ceil(rows/2))-2,0,int(rows/2),0)
msgWin=curses.newwin(int(rows/2)-1,int(cols/2),1,0)
netWin=curses.newwin(int(rows/2)-1,int(math.ceil(cols/2)),1,int(cols/2))
mainWin.scrollok(True)
msgWin.scrollok(True)
netWin.scrollok(True)
inputWin.bkgd(' ', curses.color_pair(1))
infoWin.bkgd(' ', curses.color_pair(2))
mainWin.bkgd(' ', curses.color_pair(3))
msgWin.bkgd(' ', curses.color_pair(4))
netWin.bkgd(' ', curses.color_pair(5))
infoWin.addstr(0,0,str(curses.LINES)+" "+str(curses.COLS))
stdscr.refresh()
infoWin.refresh()
mainWin.refresh()
inputWin.refresh()
msgWin.refresh()
netWin.refresh()
################################################################
#config setup
import configparser
config = configparser.ConfigParser()
wl = configparser.ConfigParser()
bl = configparser.ConfigParser()
config.read("config.ini")
wl.read("whitelist.ini")
bl.read("blacklist.ini")
class collections:
    userList = []
    password = False
    passphrase = ""
    connections = 0
    whitelist = False
    blacklist = False
    kill = False
collections.blacklist = config.getboolean('settings','blacklist')
if collections.blacklist == True:
    winMsg(mainWin,"server blacklist is enabled",1,int(math.ceil(rows/2))-3,0)
collections.whitelist = config.getboolean('settings','whitelist')
if collections.whitelist == True:
    winMsg(mainWin,"server whitelist is enabled",1,int(math.ceil(rows/2))-3,0)
collections.password = config.getboolean('settings','password')
if collections.password == True:
    winMsg(mainWin,"password is enabled",1,int(math.ceil(rows/2))-3,0)
    collections.passphrase = config['settings']['passphrase']
    winMsg(mainWin,"passphrase is "+collections.passphrase,1,int(math.ceil(rows/2))-3,0)

def setValue(file_path, section, key, value):
    cfg = configparser.RawConfigParser()
    cfg.read(file_path)
    cfg.set(section,key,value)
    cfgfile = open(file_path,'w')
    cfg.write(cfgfile)
    cfgfile.close()
################################################################
#Socket setup
import socket
host = ""
port = int(config['settings']['port'])

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    s.bind((host, port))
except:
    winMsg(stdscr,string,scrollAmount,row,col)
    winMsg(mainWin,"The port that you specified in the config file is in use. Please try again later.",1,int(math.ceil(rows/2))-3,0)
    inputWin.getch()
    exit()
winMsg(mainWin,"hosted on port:"+str(port),1,int(math.ceil(rows/2))-3,0)
################################################################
#threading setup
import threading
################################################################
#setting up ctrl+c
import signal
import sys
def signalHandler(sig, frame):
    curses.endwin()
    collections.kill=True
    s.shutdown()
    s.close()
    sys.exit()
signal.signal(signal.SIGINT, signalHandler)
################################################################
#misc setup
import time
################################################################
#essential functions
def globalMsg(text,type):
    for i in range(len(collections.userList)):
        try:
            collections.userList[i][1].sendall(bytes((type+str(text)), "utf-8"))
        except BrokenPipeError:
            winMsg(netWin,"Removed user:"+str(collections.userList[i][2]),1,int(math.ceil(rows/2))-3,0)
            collections.connections = collections.connections - 1
            collections.userList.pop(i)
            winMsg(netWin,"There are currently "+str(collections.connections)+" users connected",1,int(math.ceil(rows/2))-3,0)
            break
def personalMsg(text,type,conn):
    try:
        conn.sendall(bytes((type+str(text)), "utf-8"))
    except BrokenPipeError:
        for i in range(len(collections.userList)):
            if str(conn) == str(collections.userList[i][1]):
                winMsg(netWin,"Removed user:"+str(collections.userList[i][2]),1,int(math.ceil(rows/2))-3,0)
                collections.connections = collections.connections - 1
                collections.userList.pop(i)
                winMsg(netWin,"There are currently "+str(collections.connections)+" users connected",1,int(math.ceil(rows/2))-3,0)
                break
def passwordCheck(conn,addr):
    continueExecution = True
    if collections.password == True:
        personalMsg("","p",conn)
        msg = str(conn.recv(1024))[3:-1]
        if msg == collections.passphrase:
            personalMsg("","C",conn)
        else:
            personalMsg("","w",conn)
            continueExecution = False
    return continueExecution
################################################################
#Defining the thread functions
def addUsers():
    while collections.kill == False:
        s.listen(1)
        try:
            conn, addr = s.accept()
            (threading.Thread(target = clientConnection, args=(conn,addr), daemon=True)).start()
        except:
            pass
def clientPing(conn,addr):
    while True:
        kill = True
        personalMsg(str(collections.connections),"c",conn)
        time.sleep(1)
        for list in collections.userList:
            if addr in list:
                kill = False
        if kill == True:
            winMsg(netWin,"killing ping "+str(addr)+" thread",1,int(math.ceil(rows/2))-3,0)
            break
def adminInput():
    while True:
        cmd=rawInput(inputWin, 0, 0, "").lower()
        if cmd.split(" ")[0] == "whitelist":
            if cmd == "whitelist":
                winMsg(mainWin,wl['whitelist']['whitelist'],1,int(math.ceil(rows/2))-3,0)
            elif cmd == "whitelist true" or cmd == "whitelist 1" or cmd == "whitelist yes" or cmd == "whitelist enable" or cmd == "whitelist enabled":
                collections.whitelist = True
                winMsg(mainWin,"Enabled whitelist",1,int(math.ceil(rows/2))-3,0)
            elif cmd == "whitelist false" or cmd == "whitelist 0" or cmd == "whitelist no" or cmd == "whitelist disable" or cmd == "whitelist disabled":
                collections.whitelist = False
                winMsg(mainWin,"Disabled whitelist",1,int(math.ceil(rows/2))-3,0)
            else:
                if cmd.split(" ")[1] in wl['whitelist']['whitelist'].split(","):
                    setValue("whitelist.ini", "whitelist", "whitelist", wl['whitelist']['whitelist'].replace(","+cmd.split(" ")[1],""))
                    winMsg(mainWin,"Removed "+cmd.split(" ")[1]+" from whitelist",1,int(math.ceil(rows/2))-3,0)
                else:
                    setValue("whitelist.ini", "whitelist", "whitelist", wl['whitelist']['whitelist']+","+cmd.split(" ")[1])
                    winMsg(mainWin,"Added "+cmd.split(" ")[1]+" to whitelist",1,int(math.ceil(rows/2))-3,0)
                wl.read("whitelist.ini")
        elif cmd.split(" ")[0] == "blacklist":
            if cmd == "blacklist":
                winMsg(mainWin,bl['blacklist']['blacklist'],1,int(math.ceil(rows/2))-3,0)
            elif cmd == "blacklist true" or cmd == "blacklist 1" or cmd == "blacklist yes" or cmd == "blacklist enable" or cmd == "blacklist enabled":
                collections.blacklist = True
                winMsg(mainWin,"Enabled blacklist",1,int(math.ceil(rows/2))-3,0)
            elif cmd == "blacklist false" or cmd == "blacklist 0" or cmd == "blacklist no" or cmd == "blacklist disable" or cmd == "blacklist disabled":
                collections.blacklist = False
                winMsg(mainWin,"Disabled blacklist",1,int(math.ceil(rows/2))-3,0)
            else:
                if cmd.split(" ")[1] in bl['blacklist']['blacklist'].split(","):
                    setValue("blacklist.ini", "blacklist", "blacklist", bl['blacklist']['blacklist'].replace(","+cmd.split(" ")[1],""))
                    winMsg(mainWin,"Removed "+cmd.split(" ")[1]+" from blacklist",1,int(math.ceil(rows/2))-3,0)
                else:
                    setValue("blacklist.ini", "blacklist", "blacklist", bl['blacklist']['blacklist']+","+cmd.split(" ")[1])
                    winMsg(mainWin,"Added "+cmd.split(" ")[1]+" to blacklist",1,int(math.ceil(rows/2))-3,0)
                bl.read("blacklist.ini")
        else:
            winMsg(mainWin,cmd+" is not a command",1,int(math.ceil(rows/2))-3,0)
        inputWin.clear()

def clientConnection(conn,addr):
    winMsg(netWin,"connecting client "+str(addr),1,int(math.ceil(rows/2))-3,0)
    passedLists = False
    if collections.whitelist == True:
        if addr[0] in wl['whitelist']['whitelist'].split(","):
            passedLists = True
        else:
            personalMsg("","q",conn)
    elif collections.blacklist == True:
        if addr[0] in bl['blacklist']['blacklist'].split(","):
            personalMsg("","Q",conn)
        else:
            passedLists = True
    else:
        passedLists = True

    if passwordCheck(conn,addr) == True and passedLists == True:
        username = False
        collections.connections = collections.connections + 1
        collections.userList.append(["",conn,addr])
        (threading.Thread(target = clientPing, args=(conn,addr))).start()
        while True:
            kill = True
            try:
                msg = conn.recv(1024)
            except ConnectionResetError:
                pass
            if str(msg)[2] == "m":
                winMsg(msgWin,"msg from"+str(addr)+":"+str(msg)[3:-1],1,int(math.ceil(rows/2))-3,0)
                globalMsg(username+":"+(str(msg)[3:-1]),"m")
            elif str(msg)[2] == "u":
                    if username == False:
                        for i in range(len(collections.userList)):
                            if (collections.userList[i][2]) == addr:
                                collections.userList[i][0] = (str(msg)[3:-1])
                                username = collections.userList[i][0]
                                globalMsg(username+" has connected","m")
                    else:
                        for i in range(len(collections.userList)):
                            if (collections.userList[i][2]) == addr:
                                globalMsg(username+" has renamed themselves to "+str(msg)[3:-1],"m")
                                collections.userList[i][0] = (str(msg)[3:-1])
                                username = collections.userList[i][0]
            for list in collections.userList:
                if addr in list:
                    kill = False
            if kill == True:
                winMsg(netWin,"killing main "+str(addr)+" thread",1,int(math.ceil(rows/2))-3,0)
                globalMsg(username+" has disconnected","m")
                break

(threading.Thread(target = adminInput, args=(), daemon=True)).start()
addUsers()
